#include "dirscanner.h"
#include <QDir>

DirScanner::DirScanner(QObject *parent) :
    QObject(parent)
{
}

QString DirScanner::scan(QString path, QString mask="")
{
    if (path.right(1) != "/") {
        path += "/";
    }
    QDir dir = QDir(path);
    if (mask.isEmpty()) {
        mask = "*";
    }

    QStringList masks;
    masks << mask;

    QStringList files = dir.entryList(masks, QDir::Files, QDir::Name);
    files.sort();

    if (files.count() > 0) {
        return files.last();
    }

    return QString("");
}


/** rx-parameter must has an RegExpr pattern, which strongly identifies:
  *     the base of the filename, place of counter and the ending of the filename.
  * I.E.: rx = QRegExp("^(\\s+)(\\d+)(\\.png)$")
  *       where
  *         (\\s+) - the base of the filename (the first parameter)
  *         (\\d+) - the counter (the second parameter)
  *         (\\.png) - the extention of the filename (the third parameter)
  */
QString DirScanner::nextFileByMask(QString path, QString mask, QRegExp rx, quint8 base, QString firstFName)
{
    if (path.right(1) != "/") {
        path += "/";
    }
    QString lastFile = DirScanner::scan(path, mask);
    if (lastFile.isEmpty()
        ||
        rx.indexIn(lastFile) == -1
    ) {
        return path + firstFName;
    } else {

    }

    QString defCounter;
    for (quint8 i = 0; i < base; i++) defCounter += "0";

    QString fileName;

    if (rx.indexIn(lastFile) == -1) {
        fileName = path + firstFName; //this case could not be ever
        //path + "/" + rx.cap(1) + QString(defCounter+"1").right(base) + rx.cap(3);
    } else {
        /*
        QString fnumstr = "0000";
        int fnum = rx.cap(2).toInt()+1;
        if (fnum > 9999) {
            fnum = 1;
        }
        fnumstr += QString("%1").arg(fnum);
        */
        fileName = path + rx.cap(1) + QString(defCounter + QString("%1").arg(rx.cap(2).toInt()+1)).right(base) + rx.cap(3);
    }

    return fileName;
}
