#include "passworddialog.h"
#include "ui_passworddialog.h"

#include <QTimer>

PasswordDialog::PasswordDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswordDialog)
{
    ui->setupUi(this);

    QTimer::singleShot(50,this,SLOT(on_afterOpen()));
}

PasswordDialog::~PasswordDialog()
{
    delete ui;
}

QString PasswordDialog::getPassword()
{
    if (this->result() == QDialog::Accepted)
        return ui->passwordCheck->text();
    else
        return "";
}

void PasswordDialog::on_afterOpen()
{
    ui->passwordCheck->grabKeyboard();
}
