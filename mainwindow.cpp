#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QxtGui/QxtGlobalShortcut>
#include <QxtGui/QxtApplication>
#include <QDesktopWidget>
#include <QRegExp>
#include <QFileDialog>
#include <QCryptographicHash>
#include <QMessageBox>
#include <QCloseEvent>

#include "dirscanner.h"
#include "passworddialog.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    _setsStorage(new QSettings("DIOWO", "ScreenShotter", parent)),
    shortcut(0),
    timer(0),
    trayIcon(new QSystemTrayIcon(this)),
    trayMenu(new QMenu(this)),
    isCloseable(false)
{
    ui->setupUi(this);

    this->setCentralWidget(ui->gridLayoutWidget);
    ui->groupBox_Manual->setLayout(ui->gridLayout_Manual);
    ui->groupBox_Automatic->setLayout(ui->gridLayout_Automatic);

    this->restoreSettings();
    this->setDefaultTrayIcon();
    this->makeTrayMenu();
    this->trayIcon->setContextMenu(this->trayMenu);
    connect(this->trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(on_trayIcon_activated(QSystemTrayIcon::ActivationReason)));

   // ui->menuMenu = this->trayMenu;

    this->init();

    QTimer::singleShot(100, this, SLOT(hide()));
}

void MainWindow::setDefaultTrayIcon()
{
    //this->trayIcon->hide();
    this->trayIcon->setIcon(QIcon(":/icons/monitor.png"));
    this->trayIcon->show();
}

void MainWindow::makeTrayMenu()
{
    this->trayMenu->addAction("Exit", this, SLOT(on_closeTry()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_btn_OK_clicked()
{
    if (!this->_setsStorage->value("adminPass").toString().isEmpty()) {
        if (this->passwordCheck()) {
            qDebug() << "correct pass";
        } else {
            QMessageBox msg(this);
            msg.setText("Wrong password!");
            msg.setModal(true);
            msg.exec();
            return;
        }
    }

    this->sets.manual.shortcut = ui->le_shortcutForManualScreenshots->text();
    this->sets.manual.path = ui->le_pathForManualScreenshots->text();
    this->sets.automatic.isOn = ui->chb_takeScreenshotsAutomatically->isChecked();
    this->sets.automatic.path = ui->le_pathForAutomaticScreenshots->text();
    this->sets.automatic.period = ui->le_periodOfAutomaticScreenshots->text().toInt();

    if (this->sets.automatic.period < 3) {
        this->sets.automatic.period = 3;
        ui->le_periodOfAutomaticScreenshots->setText("3");
    }

    //TODO: save settings
    this->_setsStorage->setValue("manual/shortcut", this->sets.manual.shortcut);
    this->_setsStorage->setValue("manual/path", this->sets.manual.path);
    this->_setsStorage->setValue("automatic/isOn", this->sets.automatic.isOn);
    this->_setsStorage->setValue("automatic/path", this->sets.automatic.path);
    this->_setsStorage->setValue("automatic/period", this->sets.automatic.period);
    if (ui->le_adminPass->text() != "") {
        this->sets.adminPass = ui->le_adminPass->text();
        QString hash = QString(QCryptographicHash::hash(this->sets.adminPass.toAscii(),QCryptographicHash::Md5).toHex());
        this->_setsStorage->setValue("adminPass", hash);
    } else {
        this->sets.adminPass = "";
        QString hash = QString(QCryptographicHash::hash(this->sets.adminPass.toAscii(),QCryptographicHash::Md5).toHex());
        this->_setsStorage->setValue("adminPass", hash);
    }
    this->_setsStorage->sync();

    this->init();
}

void MainWindow::restoreSettings()
{
    this->_setsStorage->sync();
    this->sets.manual.shortcut = this->_setsStorage->value("manual/shortcut").toString();
    this->sets.manual.path = this->_setsStorage->value("manual/path").toString();
    this->sets.automatic.isOn = this->_setsStorage->value("automatic/isOn").toBool();
    this->sets.automatic.path = this->_setsStorage->value("automatic/path").toString();
    this->sets.automatic.period = this->_setsStorage->value("automatic/period").toInt();
    this->sets.adminPass = "";

    ui->le_shortcutForManualScreenshots->setText(this->sets.manual.shortcut);
    ui->le_pathForManualScreenshots->setText(this->sets.manual.path);
    ui->le_periodOfAutomaticScreenshots->setText(QString("%1").arg(this->sets.automatic.period));
    ui->le_pathForAutomaticScreenshots->setText(this->sets.automatic.path);
    ui->chb_takeScreenshotsAutomatically->setChecked(this->sets.automatic.isOn);
    ui->le_adminPass->setText(this->sets.adminPass);

    if (!this->_setsStorage->value("adminPass").toString().isEmpty()
            &&
            this->_setsStorage->value("adminPass") != QString(QCryptographicHash::hash(QString("").toAscii(),QCryptographicHash::Md5).toHex())
    ) {
        this->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint| Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint);
    } else {
        this->setWindowFlags(Qt::Window | Qt::CustomizeWindowHint | Qt::WindowTitleHint| Qt::WindowSystemMenuHint | Qt::WindowMinMaxButtonsHint | Qt::WindowCloseButtonHint);
    }
}

void MainWindow::init()
{
    //turn off
    if (this->shortcut) {
        disconnect(this->shortcut, SIGNAL(activated()), this, SLOT(on_globalShortcut()));
        delete this->shortcut;
    }
    if (this->timer) {
        disconnect(this->timer, SIGNAL(timeout()), this, SLOT(on_automaticScreenshot()));
        delete this->timer;
    }

    //turn on
    //1. manual shortcut
    this->shortcut = new QxtGlobalShortcut(this);
    connect(this->shortcut, SIGNAL(activated()), this, SLOT(on_globalShortcut()));
    this->shortcut->setShortcut(QKeySequence(this->sets.manual.shortcut));

    this->sets.manual.path = ui->le_pathForManualScreenshots->text();
    if (this->sets.manual.path.right(1) != "/") {
        this->sets.manual.path += "/";
    }

    //2. automatic screenshots
    this->sets.automatic.path = ui->le_pathForAutomaticScreenshots->text();
    if (this->sets.automatic.path.right(1) != "/") {
        this->sets.automatic.path += "/";
    }
    this->sets.automatic.period = ui->le_periodOfAutomaticScreenshots->text().toInt();
    this->sets.automatic.isOn = ui->chb_takeScreenshotsAutomatically->isChecked();

    if (this->sets.automatic.isOn && !this->sets.automatic.path.isEmpty())
    {
        this->timer = new QTimer(this);
        connect(this->timer, SIGNAL(timeout()), this, SLOT(on_automaticScreenshot()));
        this->timer->start(this->sets.automatic.period*1000);
        this->trayIcon->showMessage("ScreenShotter", "Starting to take screenshots every "+QString("%1").arg(this->sets.automatic.period)+" sec.");
    }
}

void MainWindow::on_globalShortcut()
{
    this->trayIcon->setIcon(QIcon(":/icons/monitor_screenshot.png"));
    QTimer::singleShot(500, this, SLOT(setDefaultTrayIcon()));
    //сделать скриншот
    QPixmap originalPixmap = QPixmap::grabWindow(QxtApplication::desktop()->winId());
    //сохранить его в папку
    QString format = "png";
    QString fileName = DirScanner::nextFileByMask(
                this->sets.manual.path,
                "screen*." + format,
                "^(screen)(\\d+)(\\."+format+"$)",
                4,
                "screen0001."+format
    );

    if (!fileName.isEmpty())
        originalPixmap.save(fileName, format.toAscii());

    qDebug() << "Manual screenshot " << fileName;
}

void MainWindow::on_automaticScreenshot()
{
    this->trayIcon->setIcon(QIcon(":/icons/monitor_screenshot.png"));
    QTimer::singleShot(500, this, SLOT(setDefaultTrayIcon()));
    //сделать скриншот
    QPixmap originalPixmap = QPixmap::grabWindow(QxtApplication::desktop()->winId());
    //сохранить его в папку
    QString format = "png";
    QString fileName = DirScanner::nextFileByMask(
                this->sets.automatic.path,
                "autoscreen*." + format,
                "^(autoscreen)(\\d+)(\\."+format+"$)",
                5,
                "autoscreen00001."+format
    );

    if (!fileName.isEmpty())
        originalPixmap.save(fileName, format.toAscii());

    qDebug() << "Automatic screenshot " << fileName;
}

void MainWindow::on_btn_manualBrowsePath_clicked()
{
    ui->le_pathForManualScreenshots->setText(
                QFileDialog::getExistingDirectory(
                    this,
                    tr("Storage path"),
                    (ui->le_pathForManualScreenshots->text().isEmpty() ? QDir::currentPath() : ui->le_pathForManualScreenshots->text())
                ) + "/"
        );
}

void MainWindow::on_btn_automaticBrowsePath_clicked()
{
    ui->le_pathForAutomaticScreenshots->setText(
                QFileDialog::getExistingDirectory(
                    this,
                    tr("Storage path"),
                    (ui->le_pathForAutomaticScreenshots->text().isEmpty() ? QDir::currentPath() : ui->le_pathForAutomaticScreenshots->text())
                    ) + "/"
        );
}

void MainWindow::on_trayIcon_activated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::DoubleClick:
        if (this->isVisible()) {
            this->hide();
        } else {
            this->show();
        }
        break;
    case QSystemTrayIcon::MiddleClick:
        //this->trayIcon->setIcon(QIcon(":/icons/monitor_wallpaper.png"));
    default:
        ;
    }
}


bool MainWindow::passwordCheck()
{
    if (this->_setsStorage->value("adminPass")
        ==
        QString(QCryptographicHash::hash(QString("").toAscii(),QCryptographicHash::Md5).toHex())
    ) {
        return true;
    }

    PasswordDialog *dlg = new PasswordDialog(this);

    dlg->setModal(true);
    dlg->exec();

    //while (this->isDlgPasswordCheckOpen) {}; //waits...

    QString passwordCheck = dlg->getPassword();

    delete dlg;

    return
        this->_setsStorage->value("adminPass")
        ==
        QString(QCryptographicHash::hash(passwordCheck.toAscii(),QCryptographicHash::Md5).toHex())
    ;
}

void MainWindow::on_btn_Cancel_clicked()
{
    //this->passwordCheck();
    this->restoreSettings();
}

void MainWindow::on_closeTry()
{
    if (!this->_setsStorage->value("adminPass").toString().isEmpty()
            &&
            this->_setsStorage->value("adminPass") != QString(QCryptographicHash::hash(QString("").toAscii(),QCryptographicHash::Md5).toHex())
    ) {
        if (this->passwordCheck()) {
            this->isCloseable = true;
            qDebug() << "correct pass";
            //this->close();
        } else {
            QMessageBox msg(this);
            msg.setText("Wrong password!");
            msg.setModal(true);
            msg.exec();
            this->isCloseable = false;
        }
    } else {
        this->isCloseable = true;
        //this->close();
    }
    emit this->close();
}

void MainWindow::closeEvent(QCloseEvent *eClose)
{
    if (!this->isCloseable) {
        qDebug() << "couldnot exit";
        eClose->ignore();
    }
}
