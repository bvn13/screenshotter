#include <QtGui/QApplication>
#include "mainwindow.h"
#include <QxtGui/QxtApplication>

int main(int argc, char *argv[])
{
    QxtApplication a(argc, argv);
    MainWindow w;
    w.show();
    
    return a.exec();
}
