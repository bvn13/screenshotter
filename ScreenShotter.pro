#-------------------------------------------------
#
# Project created by QtCreator 2012-10-17T12:10:15
#
#-------------------------------------------------
CONFIG   += Qxt
QXT      += core qui
QT       += core gui

TARGET = ScreenShotter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    shortcutlineedit.cpp \
    dirscanner.cpp \
    passworddialog.cpp

HEADERS  += mainwindow.h \
    shortcutlineedit.h \
    dirscanner.h \
    passworddialog.h

FORMS    += mainwindow.ui \
    passworddialog.ui


#LIBS += C:\Qxt\lib\libQxtCored.a\
#    C:\Qxt\lib\libQxtGuid.a\

LIBS += /usr/lib64/libQxtCore.so \
        /usr/lib64/libQxtGui.so

INCLUDEPATH += /home/bvn13/develope/_qt/libqxt-0.6.2/include \
                /home/bvn13/develope/_qt/libqxt-0.6.2/include/QxtCore \
                /home/bvn13/develope/_qt/libqxt-0.6.2/include/QxtGui

RESOURCES += \
    recources.qrc
