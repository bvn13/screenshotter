#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QxtGui/QxtGlobalShortcut>
#include <QTimer>
#include <QSystemTrayIcon>
#include <QMenu>

#include "shortcutlineedit.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    void init();
    void restoreSettings();
    void makeTrayMenu();
    bool passwordCheck();

protected:
    void closeEvent(QCloseEvent *);

private slots:
    void on_btn_OK_clicked();
    void on_globalShortcut();
    void on_automaticScreenshot();
    void setDefaultTrayIcon();
    void on_trayIcon_activated(QSystemTrayIcon::ActivationReason reason);
    void on_closeTry();

    void on_btn_manualBrowsePath_clicked();

    void on_btn_automaticBrowsePath_clicked();

    void on_btn_Cancel_clicked();

signals:
    void signal_automaticScreenshot();


private:
    Ui::MainWindow *ui;
    QSettings *_setsStorage;
    struct {
        struct {
            QString shortcut;
            QString path;
        } manual;
        struct {
            bool isOn;
            quint8 period;
            QString path;
        } automatic;
        QString adminPass;
    } sets;

    QxtGlobalShortcut *shortcut;
    QTimer *timer;
    QSystemTrayIcon *trayIcon;
    QMenu *trayMenu;
    bool isCloseable;
};

#endif // MAINWINDOW_H
