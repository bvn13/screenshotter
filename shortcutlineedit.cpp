#include "shortcutlineedit.h"
#include <QShortcut>
#include <QKeyEvent>
#include <QFocusEvent>
#include <QKeySequence>

#include <QtDebug>


ShortcutLineEdit::ShortcutLineEdit(QWidget *parent) :
    QLineEdit(parent)
{
}

void ShortcutLineEdit::keyPressEvent(QKeyEvent *e)
{
    QKeySequence *kseq = new QKeySequence(e->modifiers() + e->key());
    QLineEdit::setText(kseq->toString());
    delete kseq;
}
