#ifndef DIRSCANNER_H
#define DIRSCANNER_H

#include <QObject>
#include <QRegExp>

class DirScanner : public QObject
{
    Q_OBJECT
public:
    explicit DirScanner(QObject *parent = 0);
    static QString scan(QString /* path */, QString /* file mask, default = "*" */);
    static QString nextFileByMask(
            QString path /* path */,
            QString mask /* mask for search */,
            QString regexp /* regexpr for count */,
            quint8  base /* count of digits in counter */,
            QString firstFName /* first file name */
            ) {
        return DirScanner::nextFileByMask(path, mask, QRegExp(regexp), base, firstFName);
    };
    static QString nextFileByMask(
            QString /* path */,
            QString /* mask for search */,
            QRegExp /* regexpr for count */,
            quint8  /* count of digits in counter */,
            QString /* first file name */
            );
signals:
    
public slots:


};

#endif // DIRSCANNER_H
