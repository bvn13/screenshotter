#-------------------------------------------------
#
# Project created by QtCreator 2012-10-17T12:10:15
#
#-------------------------------------------------
CONFIG   += Qxt
QXT      += core qui
QT       += core gui

TARGET = ScreenShotter
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    shortcutlineedit.cpp \
    dirscanner.cpp \
    passworddialog.cpp

HEADERS  += mainwindow.h \
    shortcutlineedit.h \
    dirscanner.h \
    passworddialog.h

FORMS    += mainwindow.ui \
    passworddialog.ui

LIBS += C:\Qxt\lib\libQxtCore.a\
    C:\Qxt\lib\libQxtGui.a

RESOURCES += \
    recources.qrc
